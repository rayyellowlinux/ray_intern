#!/bin/bash

# This script will gather hosts facts

echo "########" > "/tmp/$HOSTNAME.txt"
echo "Hostname" >> "/tmp/$HOSTNAME.txt"
hostname >> "/tmp/$HOSTNAME.txt"
echo "" >> "/tmp/$HOSTNAME.txt"

echo "###################" >> "/tmp/$HOSTNAME.txt"
echo "IP Addresses" >> "/tmp/$HOSTNAME.txt"
ip a | grep inet  >> "/tmp/$HOSTNAME.txt"
echo "" >> "/tmp/$HOSTNAME.txt"

echo "###################" >> "/tmp/$HOSTNAME.txt"
echo "MAC  Addresses" >> "/tmp/$HOSTNAME.txt"
cat /sys/class/net/*/address  >> "/tmp/$HOSTNAME.txt"
echo "" >> "/tmp/$HOSTNAME.txt"


echo "#############" >> "/tmp/$HOSTNAME.txt"
echo "Number of CPU" >> "/tmp/$HOSTNAME.txt"
lscpu | grep "CPU(s):"  >> "/tmp/$HOSTNAME.txt"
lscpu | grep "Model name" >> "/tmp/$HOSTNAME.txt"
echo "" >> "/tmp/$HOSTNAME.txt"


echo "######" >> "/tmp/$HOSTNAME.txt"
echo "Memory" >> "/tmp/$HOSTNAME.txt"
free -h  >> "/tmp/$HOSTNAME.txt"
echo "" >> "/tmp/$HOSTNAME.txt"


echo "#######################" >> "/tmp/$HOSTNAME.txt"
echo "Manufactuer && Serial Number" >> "/tmp/$HOSTNAME.txt"
sudo dmidecode -t system | grep Manufacturer >> "/tmp/$HOSTNAME.txt"
sudo dmidecode -t system | grep "Serial Number" >> "/tmp/$HOSTNAME.txt"
echo

Add date >> "/tmp/$HOSTNAME.txt"
